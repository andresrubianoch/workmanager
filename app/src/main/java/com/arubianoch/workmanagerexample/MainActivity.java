package com.arubianoch.workmanagerexample;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkStatus;

public class MainActivity extends AppCompatActivity {

    private static final String WORKER_NOTIFICATION_TAG = "com.arubianoch.workmanagerexample.WORKER_NOTIFICATION_TAG";
    private static final String WORKER_PERIODICALLY_NOTIFICATION_TAG = "com.arubianoch.workmanagerexample.WORKER_PERIODICALLY_NOTIFICATION_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Constraints help you to run new task only when the device check some conditions*/
        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(true)
                .setRequiresDeviceIdle(true)
                .build();

        /*To send data you need to create an instance of Data class, put appropriate data and attach
        it to the OneTimeWorkRequest. OneTimeWorkRequest.Builder provides a method named setInputData
        that takes in the Data object.
         */
        Data data = new Data.Builder()
                .putString(MyWorker.EXTRA_TITLE, "Message from Activity!")
                .putString(MyWorker.EXTRA_TEXT, "Hi! I have come from activity.")
                .build();

        final OneTimeWorkRequest oneTimeWorkRequest = new OneTimeWorkRequest.Builder(MyWorker.class)
                .setInputData(data)
                .setConstraints(constraints)
                .addTag(WORKER_NOTIFICATION_TAG)
                .build();

        final WorkManager workManager = WorkManager.getInstance();

        findViewById(R.id.simpleWorkButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (workManager != null) {
                    workManager.enqueue(oneTimeWorkRequest);
                }
            }
        });

        final TextView textViewStatus = findViewById(R.id.textViewStatus);

        //We can know what's WorkManager's status with their id.
        //WorkManager is a LiveData and we can registered to receive that data
        if (workManager != null) {
            workManager.getStatusById(oneTimeWorkRequest.getId()).observe(this,
                    new Observer<WorkStatus>() {
                        @Override
                        public void onChanged(@Nullable WorkStatus workStatus) {
                            if (workStatus != null) {
                                textViewStatus.append("SimpleWorkRequest: " + workStatus.getState().name() + "\n");

                                if (workStatus.getState().isFinished()) {
                                    String message = workStatus.getOutputData()
                                            .getString(MyWorker.EXTRA_OUTPUT_MESSAGE,
                                                    "Default param message");

                                    textViewStatus.append("SimpleWorkRequest (Data): " + message);
                                }
                            }
                        }
                    });
        }

        //Cancelling worker job by ID. We should get work request id and use it on Work.cancelWorkById
        final UUID workId = oneTimeWorkRequest.getId();
        findViewById(R.id.cancellingJobById).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (workManager != null)
                    workManager.cancelWorkById(workId);
            }
        });

        //Cancelling worker by tag. We can add a tag for every worker we created. That tag could be
        //use for many worker. Thus, we can cancel many worker at the same time. Good approach to use
        //this is when we want to cancel a bunch of image that has been uploaded.
        findViewById(R.id.cancellingJobByTag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (workManager != null) {
                    workManager.cancelAllWorkByTag(WORKER_NOTIFICATION_TAG);
                }
            }
        });

        //Periodic work does some task run in every time we would like to do
        //This example will be execute every 15 minutes. This is the minimum time, I mean,if we
        //want to set up another time, it should be upper than this.
        final PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(
                MyWorker.class, PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, TimeUnit.MINUTES)
                .addTag(WORKER_PERIODICALLY_NOTIFICATION_TAG)
                .build();

        findViewById(R.id.addPeriodicWork).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (workManager != null) {
                    workManager.enqueue(periodicWorkRequest);
                }
            }
        });

        //TODO Add worker status and cancel periodically workers

        //Run multiple workers
        //We can add many work request that will be execute one after other
        if (workManager != null) {
            workManager.beginWith(oneTimeWorkRequest)
                    //then(other worker going here)
                    .enqueue();
        }
    }
}
